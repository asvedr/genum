package genum

func MakeFromInt[V IToInt]() map[int]V {
	var stub V
	result := map[int]V{}
	for _, v_any := range stub.GetVariants() {
		v := v_any.(V)
		result[v.Int()] = v
	}
	return result
}

func MakeFromString[V IToString]() map[string]V {
	var stub V
	result := map[string]V{}
	for _, v_any := range stub.GetVariants() {
		v := v_any.(V)
		result[v.String()] = v
	}
	return result
}
