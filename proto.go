package genum

type IToString interface {
	String() string
	GetVariants() []any
}

type IToInt interface {
	Int() int
	GetVariants() []any
}
